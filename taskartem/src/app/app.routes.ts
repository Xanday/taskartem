import { Routes } from '@angular/router';
import {PageIsNotFoundComponent} from "./404/404.component";
import {TableComponent} from "./table/table.component";

export const routes: Routes = [
  {path: 'table/:page', component: TableComponent},
  {path: '', redirectTo: '/table/1', pathMatch: "full"},
  {path: '**', component: PageIsNotFoundComponent}
];
