export interface interfaceData {
  postId: number,
  id: number,
  name: string,
  email: string,
  body: string,
}
