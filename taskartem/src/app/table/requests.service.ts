import { Injectable } from '@angular/core';
import {interfaceData} from "./interface";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable()
export class RequestsService {

  private data:interfaceData[] = [];

  constructor(private http: HttpClient) { }

  reqData(page: number, callback:Function):void {
    if(this.data.length == 0) {
      for (let i = page*10-9; i <= page*10; i++) {
        this.http.get(`https://jsonplaceholder.typicode.com/comments?id=${i}`).subscribe((value:any) => {
          this.data.push(...value);
          if(page*10 == i) {
            setTimeout(() => {
              callback(this.data);
            }, 1000)
          }
        });
      }
    } else callback(this.data);
  }
}
