import {Injectable, Optional} from '@angular/core';
import {RequestsService} from "./requests.service";
import {interfaceData} from "./interface";

@Injectable()
export class TreatmentService {

  private data:interfaceData[] = [];

  constructor(private request: RequestsService) {}

  getData(input: string, page:number, callback:Function) {
    this.request.reqData(page, (data: interfaceData[]) => {
      callback(data.filter((element:interfaceData) => {
        return element.email!.indexOf(input) != -1;
      }).sort((a, b) => {if(a.id > b.id) return 1; else return -1;}) || []);
    });
  }
}
