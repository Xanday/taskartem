import {Component, ContentChild, ElementRef, EventEmitter, ViewChild} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {MatTableModule} from "@angular/material/table";
import {FormsModule, NgModel, ReactiveFormsModule} from "@angular/forms";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {TreatmentService} from "./treatment.service";
import {HttpClientModule} from "@angular/common/http";
import {RequestsService} from "./requests.service";
import {interfaceData} from "./interface";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatSelectModule} from "@angular/material/select";
import {CommonModule} from "@angular/common";

@Component({
  selector: 'app-table',
  standalone: true,
  imports: [CommonModule, MatTableModule, FormsModule, MatFormFieldModule, MatInputModule, ReactiveFormsModule, HttpClientModule, MatFormFieldModule, MatSelectModule],
  providers: [TreatmentService, RequestsService],
  templateUrl: './table.component.html',
  styleUrl: './table.component.scss',
})
export class TableComponent {
  public data: interfaceData[] = [];
  public displayedColumns: string[] = ['postId', 'id', 'name', 'email', 'body'];
  constructor(private activateRoute: ActivatedRoute, private manageData: TreatmentService) {
    this.searchFunc();
  }

  click() {

  }

  searchFunc(event?: any) {
    console.log(event);
    this.manageData.getData( '', this.activateRoute.snapshot.params['page'], (data:interfaceData[]) => {
      this.data = data;
    })
  }
}
